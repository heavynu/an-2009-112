{
  gROOT->Reset();
  //TFile myfile("zsample-HEE.root");
  TFile myfile("hist1500-e.root");

  TH1D* h53 = (TH1D*)myfile.Get("30053;1");
  TH1D* h54 = (TH1D*)myfile.Get("30054;1");

  c1 = new TCanvas("zsample"," ",200,10,900,600);
  pad1 = new TPad("pad1","This is pad1",0.,0.,0.5,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->Draw();
  pad2 = new TPad("pad2","This is pad2",0.5,0.,1.,1.,0);
  pad2->SetFillColor(0);
  pad2->SetBorderMode(0);
  pad2->SetBorderSize(0);
  pad2->Draw();

  pad1->cd();
  pad1->SetBottomMargin(0.12);
  pad1->SetLeftMargin(0.12);
  h53->SetTitle("");
  h53->SetTitleSize(0.05, "X");
  h53->SetTitleOffset(1.4, "Y");
  h53->SetXTitle("P_{t}^{lept}, GeV");
  h53->SetYTitle("Events/5GeV");
  h53->SetStats(false);
  h53->SetAxisRange(0.,500.,"X");
  h53->Draw();
//  TLatex* t = new TLatex();
//  t->SetTextSize(0.08);
//  t->DrawLatex(2600.,35.,"a");

  pad2->cd();
  pad2->SetBottomMargin(0.12);
  pad2->SetLeftMargin(0.12);
  h54->SetTitle("");
  h54->SetTitleSize(0.05, "X");
  h54->SetTitleOffset(1.4, "Y");
  h54->SetXTitle("min #eta - #phi dist. to jet");
  h54->SetYTitle("Events/0.1");
  h54->SetStats(false);
  h54->SetAxisRange(0.5,7.,"X");
  h54->Draw();

//  t->DrawLatex(2600.,0.8,"b");
}
