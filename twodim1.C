{
  gROOT->Reset();

  bool BlackAndWhite = false;

  TFile myfile("twodim.root");

  c1 = new TCanvas("twodim1"," ",200,10,700,780);
  pad1 = new TPad("pad1","This is pad1",0.,0.,1.,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->Draw();
  pad1->cd();
  pad1->SetBottomMargin(0.12);
  pad1->SetLeftMargin(0.15);

  TH1D* h9500 = (TH1D*)myfile.Get("9500;1");
  TH1D* h8500 = (TH1D*)myfile.Get("8500;1");

  h8500->SetTitle("");
  h8500->SetTitleSize(0.05, "X");
  h8500->SetTitleSize(0.05, "Y");
  h8500->SetTitleOffset(1.05, "X");
  h8500->SetTitleOffset(1.55, "Y");
  h8500->SetXTitle("M_{W_{R}}^{cand}, GeV");
  h8500->SetYTitle("M_{N}^{cand}, GeV");
  h8500->SetStats(false);
//  h9500->SetAxisRange(0.,3000.,"X");
  h8500->Draw("BOX");

//  if(!BlackAndWhite) {
//    h8487->SetFillColor(2);
//    h8487->SetLineColor(2);
//  }
//  else {h8487->SetFillColor(1);}
//  h8487->Draw("SAME");;
}
