void limitwr()
{
  #define DIM(x) (sizeof(x)/sizeof(*x))
  
  // limit for 100pb points:
  //                                        nsig_0.95   nsig_original
  // dstsign2000-500e.d             0.75        7.93898
  // dstsign2000-1200e.d            2.75        4.54068
  // dstsign2000-300e.d             0.35        5.07744
  // dstsign1500-1100e.d            3           8.16196
  // dstsign1500-1100e-allcombin.d  4.8         16.3239  
  
  // nsign = gR/gL * nsign-nominal
  // rescale points in such a way, that gR/gL == const
  // gR proportional to xsection
  // algorithm for point 2000 - 500:
  //  find xsection of 2TeV WR (fig. 2 in note) = ~ 2 pb
  //  take into account systematics by 'valenki' method:
  //  our systematics uncertancy ~ 15%
  //  nsig_original_syst = 0.85*7.93898 = 6.748
  //  calc rescale factor = nsig/nsig_nominal_syst = 0.75/6.748 = 0.1111
  //  find WR mass for which xsection = 2 pb * 0.1111 = 0.222 pb  @  3 TeV
  //  rescale mass of NR in the proportion as WR rescaled:
  //  NR = 500 * (3 / 2) = 750 GeV
  // finally we have:
  // 2000 500  (2 pb) ->  3000 750  (0.222 pb)
  // 2000 1200  (2 pb) -> 2100 1260
  // 2000 300 (2 pb) -> 3200 480  (0.162 pb)
  // 1500 1100  (8 pb) -> 1800 1320 (3.46 pb)

  Float_t wr100pb[] = {1001, 1200, 1350, 1510, 1450, 1300, 1200, 1001};
  Float_t nr100pb[] = { 150, 200, 280,  600,  650, 710, 700, 620};
  
  
//  setTDRStyle();

  c1 = new TCanvas("limitwr", "limitwr", 200, 10, 700, 780);
  pad1 = new TPad("pad1","This is pad1",0.,0.,1.,1.,0);
//  pad1->SetFillColor(0);
//  pad1->SetBorderMode(0);
//  pad1->SetBorderSize(0);
  pad1->Draw();
  pad1->cd();
  pad1->SetLeftMargin(0.19); // to get place for Y Axis Title
//  pad1->SetTopMargin(0.02);
  pad1->SetRightMargin(0.05);
  TH1F* vFrame = pad1->DrawFrame(1000., 0., 3000., 2700.);
  vFrame->SetXTitle("M_{W_{R}}, GeV");
//  vFrame->SetLabelOffset(0.001, "Y");
  vFrame->SetTitleOffset(1.8, "Y");
  vFrame->SetYTitle("M_{N_{l}}, GeV");
  vFrame->GetXaxis()->SetNdivisions(505);

  TText* mark = new TText(0.5, 0.5, "CMS preliminary");
  mark->SetNDC(kTRUE);
  mark->SetTextColor(17);
  mark->SetTextAlign(22); // centered
  mark->SetTextSize(0.10);
  mark->SetTextAngle(45);
  mark->Draw();

  TLine* line = new TLine();
  double wl = line->GetLineWidth();
  line->SetLineWidth(2.*wl);
  line->DrawLine(1000., 1000., 2700., 2700.);

  TLatex* t = new TLatex();
  t->SetTextSize(0.04);
  t->DrawLatex(1200.,2500.,"Virtual N ");
  t->DrawLatex(1200.,2200.,"M_{N_{l}} > M_{W_{R}}");

  line = new TLine();
  wl = line->GetLineWidth();
  line->SetLineWidth(2.*wl);
  line->DrawLine(1000., 215., 3500., 215.);

  TLatex* t = new TLatex();
  t->SetTextSize(0.025);
  t->DrawLatex(2400.,110.,"Excluded by L3 (LEP)");
  
  TGraph* gr = new TGraph(DIM(wr100pb), wr100pb, nr100pb); // ---------------  100 pb**-1
  gr->SetLineColor(kGreen);
  double wl2 = gr->GetLineWidth();
  gr->SetLineWidth(2.*wl2);
  gr->Draw("C");
}
