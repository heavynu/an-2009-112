void sensitwr()
{
  Float_t arg100pb[8] = {1001., 1150., 1200., 1250., 1340., 1390., 1480., 1510.};
  Float_t s100pb[8] = {590., 640., 655., 670., 650., 630., 545., 500.};
  Float_t arg100pb1[8] = {1001., 1200., 1250., 1380., 1430., 1480., 1495., 1510.};
  Float_t s100pb1[8] = {170., 230., 250., 305., 350., 410., 430., 500.};

//  setTDRStyle();

  c1 = new TCanvas("sensitwr"," ",200,10,700,780);
  pad1 = new TPad("pad1","This is pad1",0.,0.,1.,1.,0);
//  pad1->SetFillColor(0);
//  pad1->SetBorderMode(0);
//  pad1->SetBorderSize(0);
  pad1->Draw();
  pad1->cd();
  pad1->SetLeftMargin(0.19); // to get place for Y Axis Title
//  pad1->SetTopMargin(0.02);
  pad1->SetRightMargin(0.05);
  TH1F* vFrame = pad1->DrawFrame(1000., 0., 3000., 2700.);
  vFrame->SetXTitle("M_{W_{R}}, GeV");
//  vFrame->SetLabelOffset(0.001, "Y");
  vFrame->SetTitleOffset(1.8, "Y");
  vFrame->SetYTitle("M_{N_{l}}, GeV");
  vFrame->GetXaxis()->SetNdivisions(505);

  TText* mark = new TText(0.5, 0.5, "CMS preliminary");
  mark->SetNDC(kTRUE);
  mark->SetTextColor(17);
  mark->SetTextAlign(22); // centered
  mark->SetTextSize(0.10);
  mark->SetTextAngle(45);
  mark->Draw();

  TLine* line = new TLine();
  double wl = line->GetLineWidth();
  line->SetLineWidth(2.*wl);
  line->DrawLine(1000., 1000., 2700., 2700.);

  TLatex* t = new TLatex();
  t->SetTextSize(0.04);
  t->DrawLatex(1200.,2500.,"Virtual N ");
  t->DrawLatex(1200.,2200.,"M_{N_{l}} > M_{W_{R}}");

  line = new TLine();
  wl = line->GetLineWidth();
  line->SetLineWidth(2.*wl);
  line->DrawLine(1000., 215., 3500., 215.);

  TLatex* t = new TLatex();
  t->SetTextSize(0.025);
  t->DrawLatex(2200.,110.,"Excluded by L3 (LEP)");

  gr = new TGraph(8,arg100pb,s100pb);
  gr->SetLineColor(2);
  double wl2 = gr->GetLineWidth();
  gr->SetLineWidth(2.*wl2);
  gr->Draw("C");

  gr = new TGraph(8,arg100pb1,s100pb1);
  gr->SetLineColor(2);
  wl2 = gr->GetLineWidth();
  gr->SetLineWidth(2.*wl2);
  gr->Draw("C");
}
