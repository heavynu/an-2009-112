{
  gROOT->Reset();
  TFile myfile("hist-fullreco-mu2.root");

  TH1D* h30075 = (TH1D*)myfile.Get("30075;1");
  TH1D* h30076 = (TH1D*)myfile.Get("30076;1");

  c1 = new TCanvas("isovalue-mu"," ",200,10,900,600);
  pad1 = new TPad("pad1","This is pad1",0.,0.5,1.,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->Draw();
  pad2 = new TPad("pad2","This is pad2",0.,0.,1.,0.5,0);
  pad2->SetFillColor(0);
  pad2->SetBorderMode(0);
  pad2->SetBorderSize(0);
  pad2->Draw();

  pad1->cd();
  pad1->SetBottomMargin(0.12);
  pad1->SetLeftMargin(0.12);
  h30075->SetTitle("");
  h30075->SetTitleSize(0.05, "X");
  h30075->SetTitleOffset(1.4, "Y");
  h30075->SetXTitle("isovalue");
  h30075->SetYTitle("leptons");
  h30075->SetAxisRange(0.,40.,"X");
  h30075->Draw();
//  TLatex* t = new TLatex();
//  t->SetTextSize(0.08);
//  t->DrawLatex(2600.,35.,"a");

  pad2->cd();
  pad2->SetBottomMargin(0.12);
  pad2->SetLeftMargin(0.12);
  h30076->SetTitle("");
  h30076->SetTitleSize(0.05, "X");
  h30076->SetTitleOffset(1.4, "Y");
  h30076->SetXTitle("isovalue");
  h30076->SetYTitle("leptons");
  h30076->SetAxisRange(0.,40.,"X");
  h30076->Draw();
}
