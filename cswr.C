#include "TROOT.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMultiGraph.h"

void cswr()
{
  const bool BlackAndWhite = false;
  
  Float_t arg[5] = {1., 1.5, 2., 3., 4.};
  Float_t cs14[5] = {3.91e-8, 7.232e-9, 1.879e-9, 2.127e-10, 3.233e-11};
  Float_t cs10[5] = {1.909e-8, 2.958e-9, 6.383e-10, 4.643e-11, 5.699e-12};
  Float_t cs7[4] = {7.948e-9, 9.269e-10, 1.436e-10, 7.124e-12};
  
  // from mb to pb
  for(int i=0; i<5; i++) {cs14[i] *= 1.e9; cs10[i] *= 1.e9;}
  for(int i=0; i<4; i++) {cs7[i] *= 1.e9;}
  
  gROOT->SetStyle("Plain");
  
  TCanvas* canvas = new TCanvas("cswr", "cswr", 200, 10, 700, 780);
  // log scale along Y axis
  canvas->SetLogy();
  // margins: left rigth bottom top
  canvas->SetMargin(0.12, 0.02, 0.12, 0.02);
  
  TGraph* gr14 = new TGraph(5,arg,cs14);
  if(!BlackAndWhite) gr14->SetLineColor(kRed);
  gr14->SetLineWidth(3);
  
  TGraph* gr10 = new TGraph(5,arg,cs10);
  if(!BlackAndWhite) gr10->SetLineColor(kGreen);
  gr10->SetLineWidth(3);
  
  TGraph* gr7 = new TGraph(4,arg,cs7);
  if(!BlackAndWhite) gr7->SetLineColor(kBlue);
  gr7->SetLineWidth(3);
  
  TMultiGraph* mg = new TMultiGraph("xs", ";M_{W_{R}}, TeV/c^{2};#sigma(pp #rightarrow W_{R}), pb");
  // mg->SetTitle("Signal cross section: heavy neutrino from right-handed W");
  mg->Add(gr14);
  mg->Add(gr10);
  mg->Add(gr7);
  mg->Draw("AC");
  
  // range for X and Y axis
  mg->GetXaxis()->SetLimits(0, 6);
  mg->GetYaxis()->SetRangeUser(1e-3, 60);
  // increase font size of X and Y axis
  TH1F* frame = mg->GetHistogram();
  frame->SetTitleSize(0.05, "XY");
  
  // legend
  TLegend* legend = new TLegend(0.75, 0.8, 0.95, 0.95);
  legend->SetFillColor(0);
  legend->AddEntry(gr14, "14 TeV", "L");
  legend->AddEntry(gr10, "10 TeV", "L");
  legend->AddEntry(gr7,   "7 Tev", "L");
  legend->Draw();
  
  canvas->Modified();
  canvas->Print("cswr.eps");
}
