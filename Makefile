
# to get filenames of all plots: cat heavynu.tex | grep -o '[a-z0-9.=_-]*\.eps'

PLOTS=eff1.eps cswr.eps

all: heavynu.pdf

heavynu.pdf: $(PLOTS) heavynu.tex
	latex heavynu.tex
	latex heavynu.tex
	dvipdf heavynu.dvi

eff1.eps: eff1.C
	root -b -q -l eff1.C

cswr.eps: cswr.C
	root -b -q -l cswr.C
