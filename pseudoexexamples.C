{
  gROOT->Reset();

  bool BlackAndWhite = false;
  TFile myfile("fit-data=dstsign1200-500e.d-samples=1000-2D.root");

  c1 = new TCanvas("pseudoexexamples"," ",200,10,900,600);
  pad1 = new TPad("pad1","This is pad1",0.,0.,1.,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->Draw();

  TH1D* h9500 = (TH1D*)myfile.Get("h1MN_MW;1");

  pad1->cd();
  pad1->SetBottomMargin(0.12);
  pad1->SetLeftMargin(0.15);
  h9500->SetTitle("");
  h9500->SetTitleSize(0.05, "X");
  h9500->SetTitleSize(0.05, "Y");
  h9500->SetTitleOffset(1.8, "X");
  h9500->SetTitleOffset(1.8, "Y");
  h9500->SetXTitle("M_{W_{R}}^{cand}, GeV");
  h9500->SetYTitle("M_{N}^{cand}/M_{W_{R}}^{cand}");
  h9500->SetStats(false);
  h9500->SetAxisRange(0.,2.1,"Z");
  h9500->Draw("LEGO");
}
