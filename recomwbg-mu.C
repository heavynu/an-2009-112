{
  gROOT->Reset();

  bool BlackAndWhite = true;

  TFile myfile("hist1500-mu.root");

  c1 = new TCanvas("recomwbg-mu"," ",200,10,700,780);
  pad1 = new TPad("pad1","This is pad1",0.,0.,1.,1.,0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->Draw();
  pad1->cd();
  pad1->SetBottomMargin(0.12);
  pad1->SetLeftMargin(0.15);

  TH1D* h9487 = (TH1D*)myfile.Get("29487;1");
  TH1D* h8487 = (TH1D*)myfile.Get("28487;1");

  h9487->SetTitle("");
  h9487->SetTitleSize(0.05, "X");
  h9487->SetTitleOffset(1.8, "Y");
  h9487->SetXTitle("M_{W_{R}}^{cand}, GeV");
  h9487->SetYTitle("Events/60GeV");
  h9487->SetStats(false);
  h9487->SetAxisRange(0.,3000.,"X");
  h9487->Draw();

  if(!BlackAndWhite) {
    h8487->SetFillColor(2);
    h8487->SetLineColor(2);
  }
  else {h8487->SetFillColor(1);}
  h8487->Draw("SAME");;
}
